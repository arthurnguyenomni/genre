import cv2
import os
import pandas as pd
from enum import Enum


Color = {'RED' : (0, 0, 255),
         'GREEN' : (0, 255, 0),
         'BLUE' : (255, 0, 0)
         }


def show_results(image_path, list_of_key_objects, output_dir):
    image = cv2.imread(image_path)
    for key_obj in list_of_key_objects:
        key_coords_x1y1 = (key_obj.x1, key_obj.y1)
        key_coords_x2y2 = (key_obj.x2, key_obj.y2)

        cv2.rectangle(image, key_coords_x1y1, key_coords_x2y2, Color.get('GREEN'), 3)

        if key_obj.value is not None:
            for value in key_obj.value:
                val_coords_x1y1 = (value.x1, value.y1)
                val_coords_x2y2 = (value.x2, value.y2)
                cv2.rectangle(image, val_coords_x1y1, val_coords_x2y2, Color.get('BLUE'), 3)
                key_center = ((key_obj.x1 + key_obj.x2) // 2, (key_obj.y1 + key_obj.y2) // 2)
                val_center = ((value.x1 + value.x2) // 2, (value.y1 + value.y2) // 2)

                cv2.line(image, key_center, val_center, Color.get('RED'), 3)

    output_image_path = os.path.join(output_dir,
                                     'key_and_val-{0}'.format(os.path.basename(image_path)))
    cv2.imwrite(output_image_path, image)


def show_text(image_path, list_of_text_objects, output_dir):
    image = cv2.imread(image_path)
    for text_obj in list_of_text_objects:
        text_coords_x1y1 = (text_obj.x1, text_obj.y1)
        text_coords_x2y2 = (text_obj.x2, text_obj.y2)

        cv2.rectangle(image, text_coords_x1y1, text_coords_x2y2, Color.get('GREEN'), 1)
        cv2.putText(image, text_obj.text, text_coords_x1y1, cv2.FONT_HERSHEY_SIMPLEX, 1, Color.get('RED'), 1, cv2.LINE_AA)

    output_image_path = os.path.join(output_dir,
                                     'text-{0}'.format(os.path.basename(image_path)))
    cv2.imwrite(output_image_path, image)


def create_gt(form, list_of_key_objects):
    df = pd.DataFrame(columns=['form', 'key', 'value', 'key_coords', 'val_coords'], index=None)
    for key_obj in list_of_key_objects:
        if key_obj.value is not None:
            for value in key_obj.value:
                key = key_obj.key
                val = value.text
                key_coords = (key_obj.x1, key_obj.y1, key_obj.x2, key_obj.y2)
                val_coords = (value.x1, value.y1, value.x2, value.y2)
                df = df.append(pd.DataFrame([[form, key, val, key_coords, val_coords]], columns=df.columns))
    return df


