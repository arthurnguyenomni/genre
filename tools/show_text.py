import pandas as pd
from PIL import Image
import boto3
from io import BytesIO
from urllib.parse import urlparse
import sys
import os
import logging


def filter_null_text_forms(full_df,
                           null_ratio_threshold=0.05):
    """Filter forms that have too many null text boxes
    Forms that are not actually complete and have a significant
    amount of empty text boxes are filtered out
    Args:
        full_df:
        null_ratio_threshold:

    Returns:

    """
    list_of_non_null_text_dfs = []
    for form, group in full_df.groupby(['form_name', 'form_page']):
        total_text_boxes = group.shape[0]
        null_text_boxes = group[pd.isnull(group['truth_value'])].shape[0]
        form_null_ratio = null_text_boxes / total_text_boxes
        if form_null_ratio > null_ratio_threshold:
            logging.error('(Form:{0}, Page:{1}) not included due to too many null text boxes.'.format(form[0], form[1]))
        else:
            list_of_non_null_text_dfs.append(group)

    return pd.concat(list_of_non_null_text_dfs)


def filter_null_text_boxes(df):
    """Removes rows with null text box predictions from df"""
    df = df.copy()
    return df[~pd.isnull(df['truth_value'])]


def filter_null_key_value_pairs(full_df, value_threshold=10):
    """Filter forms that do not have enough values
    Forms that are not actually complete and have a significant
    amount of value text boxes are filtered out
    Args:
        full_df:
        value_threshold:

    Returns:

    """
    list_of_non_null_values_dfs = []
    for form, group in full_df.groupby(['form_name', 'form_page']):
        value_text_boxes = group[~pd.isnull(group['key_name'])].shape[0]
        if value_text_boxes < value_threshold:
            logging.error('(Form:{0}, Page:{1}) not included due to not enough values.'.format(form[0], form[1]))
        else:
            list_of_non_null_values_dfs.append(group)

    return pd.concat(list_of_non_null_values_dfs)


def get_image_from_s3(url):
    """Downloads an image from s3 and returns as PIL Image."""
    url_parsed = urlparse(url)
    path = url_parsed.path
    path_split = path.split('/')
    bucket = path_split[1]
    bucket_path = '/'.join(path_split[2:])

    s3 = boto3.client('s3')
    f = BytesIO()
    s3.download_fileobj(bucket, bucket_path, f)
    img = Image.open(f)
    return img


def get_text_dump(group):
    text_lines = ''
    for row in group.itertuples():
        if row.truth_value is not None:
            line = '{text} | {x1},{y1},{x2},{y2}\n'.format(text=row.truth_value.replace('|', '') if row.truth_value is not None else '',
                                                           x1=row.x_pos, y1=row.y_pos, x2=row.x_pos + row.x_width, y2=row.y_pos + row.y_height)
            text_lines += line
    return text_lines


def get_image(group):
    image_url = group.iloc[0]['forms3link']
    rotation = group.iloc[0]['rotation_degrees']
    image = get_image_from_s3(image_url).rotate(-1*rotation)
    return image


def get_golden_truth(group, syn_df):
    df = pd.DataFrame(columns=['ZID', 'Key', 'ValueNumber', 'GoldenTruth'])
    gt = group[~pd.isnull(group['key_name'])]
    for row in gt.itertuples():
        df = df.append(
            pd.DataFrame([['{form_name}-{form_page}'.format(form_name=row.form_name,
                                                            form_page=row.form_page),
                           syn_df.get(row.key_name)['eng_value'],
                           row.value_num,
                           row.truth_value]],
                         columns=df.columns)
        )
    return df


def main():
    df = pd.read_pickle('/Users/arthurnguyen/Desktop/df.pickle')

    df = filter_null_text_forms(df)
    df = filter_null_text_boxes(df)
    df = filter_null_key_value_pairs(df)

    syn_df = pd.read_csv(
        '/Users/arthurnguyen/Desktop/Git-Omni/mega-extraction/data/materials/health_check_keys_and_translations.csv')
    syn_dict = syn_df.set_index('omni_key').to_dict(orient='index')

    for form, group in df.groupby(['form_name', 'form_page']):
        logging.info(form)
        text_dump = get_text_dump(group)
        # with open(os.path.join('/Users/arthurnguyen/Desktop/RGA-group2/text', '{0}.txt'.format('-'.join(str(e) for e in form))), 'w') as out:
        #     out.write(text_dump)

        # image = get_image(group)
        # image.save(os.path.join('/Users/arthurnguyen/Desktop/RGA-group2/image', '{0}.png'.format('-'.join(str(e) for e in form))))

        gt = get_golden_truth(group, syn_dict)
        gt.to_csv(os.path.join('/Users/arthurnguyen/Desktop/RGA-group2/gt', '{0}.csv'.format('-'.join(str(e) for e in form))), index=False)


if __name__ == '__main__':
    main()

    list_of_dfs = []
    for df_file in os.listdir('/Users/arthurnguyen/Desktop/RGA-group2/gt'):
        list_of_dfs.append(pd.read_csv(os.path.join('/Users/arthurnguyen/Desktop/RGA-group2/gt', df_file)))
    gt_df = pd.concat(list_of_dfs)
    gt_df.to_csv('/Users/arthurnguyen/Desktop/RGA-group2/gt.csv', index=False)