import os
import re
import pandas as pd
import xml.etree.ElementTree as ET


def read_xml(xml_file):
    tree = ET.parse(xml_file)
    root = tree.getroot()

    key_val_dict = {'key': [],
                    'value': []}
    for child in root:
        if child.tag == 'object':
            type_ = child.find('name').text
            x1 = int(child.find('bndbox').find('xmin').text)
            y1 = int(child.find('bndbox').find('ymin').text)
            x2 = int(child.find('bndbox').find('xmax').text)
            y2 = int(child.find('bndbox').find('ymax').text)
            coords = (x1, y1, x2, y2)

            key_val_dict[type_].append(coords)

    return key_val_dict


def inside(x_mid, y_mid, list_of_coords):
    for coords in list_of_coords:
        if coords[0] <= x_mid <= coords[2] and coords[1] <= y_mid <= coords[3]:
            return True
    return False


def batch_filter(df, input_xml_dir, type_='key'):
    all_forms = []
    for form, group in df.groupby('forms3link'):
        input_xml_file = os.path.join(input_xml_dir, '{form_name}-{form_page}.xml'.format(form_name=group.iloc[0]['form_name'],
                                                                                          form_page=group.iloc[0]['form_page']))
        try:
            filtered_df = filter_df_for_type(group, input_xml_file, type_=type_)
            all_forms.append(filtered_df)
        except:
            pass
    return pd.concat(all_forms)


def filter_df_for_type(df, xml_file, type_='key'):
    key_val_dict = read_xml(xml_file)
    df['key'] = df.apply(lambda x: inside(x.x_pos + x.x_width/2, x.y_pos + x.y_height/2, key_val_dict['key']), axis=1)
    # print(df)
    return df[df['key']]


if __name__ == '__main__':
    # read_xml('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/labels/images/5716992-lab_Redacted2-1.xml')
    df = pd.read_pickle('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/golden_truth_text.pickle')
    filtered_df = batch_filter(df,
                               '/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/labels/images')

    filtered_df.drop_duplicates('truth_value', inplace=True)
    filtered_df[filtered_df['truth_value'].apply(lambda x: not re.match('\(', x))].to_csv('/Users/arthurnguyen/Desktop/new.csv')





