import pandas as pd
import logging
logging.getLogger().setLevel(logging.DEBUG)


def compare(value_1, value_2):
    return value_1 == value_2 or str(value_2) in str(value_1)


def test_results(golden_truth_df,
                 test_df):

    combined_df = pd.merge(golden_truth_df, test_df, on=['form', 'key'], how='outer', suffixes=('_gt', '_test'), copy=False)

    combined_df['gt_exists'] = ~pd.isnull(combined_df['value_gt'])
    combined_df['test_exists'] = ~pd.isnull(combined_df['value_test'])
    combined_df['correct'] = combined_df.apply(lambda x: compare(x.value_gt, x.value_test), axis=1)

    recall_df = combined_df[combined_df['gt_exists']]
    recall_correct = recall_df['correct'].sum()
    recall_total = recall_df.shape[0]
    recall = recall_correct / recall_total

    precision_df = combined_df[combined_df['test_exists']]
    precision_correct = precision_df['correct'].sum()
    precision_total = precision_df.shape[0]
    precision = precision_correct / precision_total

    logging.info('Recall - {0}'.format(recall))
    logging.info('Precision - {0}'.format(precision))
    combined_df.to_excel('/Users/arthurnguyen/Desktop/GenRe_results.xlsx')
    return recall


def get_individual_accuracy(key_name, value, test_df):
    test_key = test_df[test_df['key'] == key_name]
    if not test_key.shape[0]:
        return False
    return value == test_key.loc[0]['value']


if __name__ == '__main__':
    gt_df = pd.read_excel('/Users/arthurnguyen/Desktop/GenRe_gt.xlsx', keep_default_na=False)
    test_df = pd.read_pickle('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/testing/test.pickle')

    syn_df = pd.read_excel('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/materials/important_keys.xlsx')
    gt_df = gt_df[gt_df['key'].isin(syn_df[syn_df['Type'] == 'Key']['Name'].tolist())]

    gt_df = gt_df[gt_df['key'].isin(syn_df['Name'].tolist())]

    test_results(gt_df, test_df)