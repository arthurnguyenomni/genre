from google.cloud import vision
from google.cloud.vision import types
import io
from google.oauth2 import service_account
import os
import json
import requests
import base64
import re
import pandas as pd

REQUEST_JSON_TEMPLATE = {
  "requests": [
    {
      "features": [
        {
          "type": "TEXT_DETECTION"
        }
      ],
      "image": {
        "source": {},
          "content": ""
      },
      "imageContext": {
        "languageHints": [
          "ja"
        ]
      }
    }
  ]
}

INPUT_TEST_IMAGES = 'test/PoC/materials/input_test_images'
OUTPUT_TEXT_FILES = 'test/PoC/materials/output_google_cloud_vision_text_dumps'

URL = 'https://vision.googleapis.com/v1/images:annotate?key='
KEY = os.environ['GOOGLE_API_KEY']  # set GOOGLE_API_KEY
URL_AND_KEY = URL + KEY


def image_to_base64(input_image):
    with open(input_image, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
        return encoded_string


def google_vision_api(input_image,
                      languageHints=["en"]):
    encoded_string = image_to_base64(input_image)
    payload = REQUEST_JSON_TEMPLATE
    payload['requests'][0]['image']['content'] = encoded_string.decode('ascii')
    payload['requests'][0]['imageContext']['languageHints'] = languageHints

    text_output_json = requests.post(URL_AND_KEY, json=payload)
    print(text_output_json.status_code)

    result_dict = json.loads(text_output_json._content)
    return result_dict


def dict_to_text_file(input_response_dict, output_file):
    total_text = ''
    for page in input_response_dict['responses'][0]['fullTextAnnotation']['pages']:
        for block in page['blocks']:
            for paragraph in block['paragraphs']:
                for word in paragraph['words']:
                    text = ''
                    for symbol in word['symbols']:
                        text += symbol['text']
                    x1 = word['boundingBox']['vertices'][0]['x']
                    y1 = word['boundingBox']['vertices'][0]['y']
                    x2 = word['boundingBox']['vertices'][2]['x']
                    y2 = word['boundingBox']['vertices'][2]['y']
                    text_line = '{0} | {1}, {2}, {3}, {4}\n'.format(text.replace('|', 'I'), x1, y1, x2, y2)
                    total_text += text_line
    with open(output_file, 'w') as output_file:
        output_file.write(total_text)


def dict_to_df(input_response_dict, output_df, image_name):
    form_name = '-'.join(image_name.split('-')[:-1])
    form_page = image_name.split('-')[-1]

    df = pd.DataFrame(columns=['truth_value', 'box', 'form_name', 'form_page', 'x_pos', 'y_pos', 'x_width', 'y_height'])

    text_boxes = iter(input_response_dict['responses'][0]['textAnnotations'])
    next(text_boxes)
    for page in input_response_dict['responses'][0]['fullTextAnnotation']['pages']:
        for block in page['blocks']:
            for paragraph in block['paragraphs']:
                for word in paragraph['words']:
                    text = ''
                    for symbol in word['symbols']:
                        text += symbol['text']

                    x1 = max(0, min([x['x'] for x in word['boundingBox']['vertices']]))
                    y1 = max(0, min([x['y'] for x in word['boundingBox']['vertices']]))
                    x2 = max(0, max([x['x'] for x in word['boundingBox']['vertices']]))
                    y2 = max(0, max([x['y'] for x in word['boundingBox']['vertices']]))

                    df = df.append(pd.DataFrame([[text, [x1, y1, x2, y2], form_name, form_page, x1, y1, x2 - x1, y2 - y1]],
                                                columns=df.columns))
    df.to_csv(output_df, index=False)


def google_image_to_text_file(input_image,
                              output_text,
                              languageHints=["ja", "en"]):
    google_response_dict = google_vision_api(input_image,
                                             languageHints=languageHints)
    dict_to_text_file(google_response_dict,
                      output_text)


def google_image_to_df(input_image,
                       output_df,
                       languageHints=["ja", "en"]):
    google_response_dict = google_vision_api(input_image,
                                             languageHints=languageHints)
    dict_to_df(google_response_dict,
               output_df,
               os.path.basename(input_image)[:-4])


def google_ocr_batch(input_image_dir,
                     output_dir,
                     languageHints=["ja", "en"],
                     how='df'):
    images = [image for image in os.listdir(input_image_dir) if re.search('(png|jpeg|jpg)$', image)]
    for image in images:
        image_path = os.path.join(input_image_dir,
                                  image)

        if how == 'text':
            text_path = os.path.join(output_dir,
                                     '{0}.txt'.format(''.join(image.split('.')[:-1])))
            google_image_to_text_file(image_path,
                                      text_path,
                                      languageHints=languageHints)
        elif how == 'df':
            df_path = os.path.join(output_dir,
                                   '{0}.csv'.format(''.join(image.split('.')[:-1])))
            google_image_to_df(image_path,
                               df_path,
                               languageHints=languageHints)


if __name__ == '__main__':
    # google_ocr_batch('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/labels/temp/',
    #                  '/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/text/google_vision',
    #                  languageHints=['en'],
    #                  how='df')

    for im in os.listdir('/Users/arthurnguyen/Desktop/astra-FIF/NOPOL/SINGARAJA'):
        response = google_vision_api(os.path.join('/Users/arthurnguyen/Desktop/astra-FIF/NOPOL/SINGARAJA', im),
                          languageHints='id')
        try:
            print("---------------")
            print(im)
            print(response)
            print(response['responses'][0]['textAnnotations'][0]['description'])
        except Exception as e:
            pass