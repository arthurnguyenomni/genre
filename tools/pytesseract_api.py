import pytesseract
import cv2
import pandas as pd
import os


OUTPUT_DIR = '/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/text/tesseract'


def image_to_text(input_image_file,
                  input_text_boxes_file):
    image = cv2.imread(input_image_file)
    text_boxes_df = pd.read_csv(input_text_boxes_file)
    for index, row in text_boxes_df.iterrows():
        im_box = image[row['y_pos']: row['y_pos'] + row['y_height'], row['x_pos']: row['x_pos'] + row['x_width']]
        text = pytesseract.image_to_string(im_box)
        text_boxes_df.loc[index, 'truth_value'] = text

    return text_boxes_df


def batch_images_to_text(input_images,
                         input_text_boxes):
    images = [image for image in os.listdir(input_images) if image.endswith('.png')]
    text_boxes = [text_box for text_box in os.listdir(input_text_boxes) if text_box.endswith('.csv')]

    for image in images:
        print(image)
        image_path = os.path.join(input_images, image)
        text_box_path = os.path.join(input_text_boxes, '{0}.csv'.format(image[:-4]))
        text_df = image_to_text(image_path, text_box_path)
        text_df.to_csv(os.path.join(OUTPUT_DIR, '{0}.csv'.format(image[:-4])))


if __name__ == '__main__':
    # image_to_text('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/labels/images/5695447-lab_Redacted-0.png',
    #               '/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/text/mega-ocr/5695447-lab_Redacted-0.csv')
    batch_images_to_text('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/labels/images',
                         '/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/text/mega-ocr')