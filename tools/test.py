import pandas as pd
import os
import cv2
import re
import regex
import logging
logging.getLogger().setLevel(logging.INFO)


def test_batch(df, input_image_dir, syn_df):
    for form, group in df.groupby('forms3link'):
        input_image_path = os.path.join(input_image_dir, '{form_name}-{form_page}.png'.format(form_name=group.iloc[0]['form_name'],
                                                                                              form_page=group.iloc[0]['form_page']))
        label_image(group, input_image_path, syn_df)
        break


def label_image(df, input_image, syn_df):
    important_df = df[df['truth_value'].apply(lambda x: match_key(x, syn_df))]

    image = cv2.imread(input_image)
    logging.info(input_image)
    for iterrow in important_df.itertuples():
        cv2.rectangle(image,
                      (iterrow.x_pos, iterrow.y_pos),
                      (iterrow.x_pos + iterrow.x_width, iterrow.y_pos + iterrow.y_height),
                      (255, 5, 5),
                      5)
    logging.info('{0}-labelled.png'.format(input_image[:-4]))
    cv2.imwrite('{0}-labelled.png'.format(input_image[:-4]), image)


def compile_with_error(phrase, avg_len=5, error=0):
    error = int(len(phrase) / avg_len)
    return regex.compile('(' + phrase + '){{e<={0}}}'.format(error))


def match_key(text, syn_df, sub_list='[ \(\)\*,\[\]]'):
    try:
        for iterrow in syn_df.itertuples():
            key = iterrow.Name
            key = re.sub(sub_list, '', key)
            r = compile_with_error(key)
            text = re.sub(sub_list, '', text)
            if r.search(text):
                # logging.info('{key}-{text}'.format(key=key, text=text))
                return True
    except:
        pass
    return False


if __name__ == '__main__':
    syn_df = pd.read_excel('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/materials/important_keys.xlsx')
    df = pd.read_pickle('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/golden_truth_text.pickle')
    test_batch(df, '/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp', syn_df)