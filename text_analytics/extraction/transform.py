import re
import regex
from rtree import index
import logging
import sys
from copy import deepcopy
import pandas as pd
import editdistance
from text_analytics.extraction.text_class import TextObject
import numpy as np

def shrink_df(df, shrink_size=1, axis=1):
    df = df.copy()

    df['x_end'] = df['x_pos'] + df['x_width']
    df['y_end'] = df['y_pos'] + df['y_height']

    if axis == 0:
        df['x_pos'] = df.apply(lambda x: x.x_pos + ((1 - shrink_size) * x.x_width), axis=1)
        df['x_end'] = df.apply(lambda x: x.x_pos + (shrink_size * x.x_width), axis=1)
        df['x_width'] = df['x_end'] - df['x_pos']

    elif axis == 1:
        df['y_pos'] = df.apply(lambda x: x.y_pos + ((1 - shrink_size) * x.y_height), axis=1)
        df['y_end'] = df.apply(lambda x: x.y_pos + (shrink_size * x.y_height), axis=1)
        df['y_height'] = df['y_end'] - df['y_pos']

    return df


def df_to_text_objects(df, shrink_size=1):
    try:
        assert 0 <= shrink_size <= 1.0
    except:
        logging.error('Shrink size out of range - {0}'.format(shrink_size))
        sys.exit()

    list_of_text_objects = [TextObject(text=iterrow.truth_value,
                                       coords=(
                                           int(iterrow.x_pos + ((1 - shrink_size) * iterrow.x_width)),
                                           int(iterrow.y_pos + ((1 - shrink_size) * iterrow.y_height)),
                                           int(iterrow.x_pos + (shrink_size * iterrow.x_width)),
                                           int(iterrow.y_pos + (shrink_size * iterrow.y_height))
                                       ),
                                       key=iterrow.key)
                            for iterrow in df.itertuples()]
    return list_of_text_objects


def compile_with_error(phrase, avg_len=4, error=0):
    error = int(len(phrase) / avg_len)
    return regex.compile('(' + phrase + '){{e<={0}}}'.format(error))


def match_key(text,
              syn_df,
              key_name='Name',
              regex_name='RegEx',
              sub_list='[ \(\)\*,\[\]]'):

    if text is None:
        return None

    edit_dist_map = {}

    found_match = False

    for iterrow in syn_df.itertuples():
        key = iterrow.RegEx
        key = re.sub(sub_list, '', key)
        r = compile_with_error(key)
        text = re.sub(sub_list, '', text)
        if r.match(text):
            found_match = True
        edit_dist = editdistance.eval(iterrow.RegEx, text)
        edit_dist_map[iterrow.Name] = min(edit_dist_map.get(iterrow.Name, edit_dist), edit_dist)

    if found_match:
        return sorted(edit_dist_map.items(), key=lambda x: x[1])[0][0]

    return None


def get_keys(df, syn_df):
    df = df.copy()
    df['key'] = df['truth_value'].apply(lambda x: match_key(x, syn_df))
    return df


def get_intersects(idx, coords):
    intersect_idx = index.Index()
    intersection_ids = list(idx.intersection((coords), objects=True))
    for i, id in enumerate(intersection_ids):
        box = (id.object.x1, id.object.y1, id.object.x2, id.object.y2)
        intersect_idx.add(i, box, obj=id.object)
    return intersect_idx


def get_nearest(idx, coords):
    return list(idx.nearest(coords, -1, objects=True))


def build_idx(list_of_text_objects):
    idx = index.Index()
    for i, text_box in enumerate(list_of_text_objects):
        text_box.box = (text_box.x1, text_box.y1, text_box.x2, text_box.y2)

        try:
            assert text_box.box[0] <= text_box.box[2] and text_box.box[1] <= text_box.box[3]
        except:
            logging.error('x1 less than x2 or y1 less than y2 - {0}'.format(text_box.__dict__))
            sys.exit()
        try:
            assert text_box.box[0] >= 0 and text_box.box[1] >= 0
        except:
            logging.error('x1 or y1 less than 0')

        idx.add(i, text_box.box, obj=text_box)
    return idx


def right_of(coords_1, coords_2):
    if coords_1[0] <= coords_2[0]:
        return True
    return False


def equivalent(coords1, coords2):
    return coords1 == coords2


def validate_value(text, pattern='\('):
    try:
        if re.match(pattern, text):
            return False
    except:
        return False
    return True


def return_value(idx, coords, num_of_vals=1):
    list_of_vals = []
    for i, text_box in enumerate(idx):
        box = (text_box.object.x1, text_box.object.y1, text_box.object.x2, text_box.object.y2)
        if not equivalent(coords, box):
            if right_of(coords, box):
                if validate_value(text_box.object.text):
                    list_of_vals.append(text_box.object)
                    if len(list_of_vals) == num_of_vals:
                        return list_of_vals

    return list_of_vals if len(list_of_vals) else None


def get_matches(list_of_text_objects, x_min, x_max):
    list_of_text_objects = deepcopy(list_of_text_objects)
    logging.info('Size of text objects: {0}'.format(len(list_of_text_objects)))
    idx = build_idx(list_of_text_objects)

    special_key_set = ['BloodPressure', 'BPM', 'IPM']
    special_test_set = ['AtRest', 'AfterExercise', '3MinutesLater']

    special_urine_blood = ['Glucose', 'Creatinine']

    def get_special(i, special_test_set):
        try:
            return special_test_set[i]
        except IndexError:
            return ""

    for text_obj in list_of_text_objects:
        if text_obj.key:
            logging.info('Text object- {0}'.format(text_obj.__dict__))
            intersect_coords = (x_min, text_obj.y1, x_max, text_obj.y2)
            key_coords = (text_obj.x1, text_obj.y1, text_obj.x2, text_obj.y2)
            possible_values = get_nearest(get_intersects(idx, intersect_coords), key_coords)
            logging.info('Number of possible values - {0}'.format(len(possible_values)))
            num_of_values = 3 if text_obj.key in special_key_set else 1
            values = return_value(possible_values, key_coords, num_of_vals=num_of_values)
            # logging.info('Key- {0}, Val- {1}'.format(text_obj.text, values))
            text_obj.value = values

    for i, text_obj in enumerate(list_of_text_objects):
        if text_obj.key in special_key_set:
            logging.info('Special key - {0}'.format(text_obj.key))
            additional_text_objects = []
            if text_obj.value is not None:
                for i, value in enumerate(text_obj.value):
                    curr_text_obj = deepcopy(text_obj)
                    curr_text_obj.key += get_special(i, special_test_set)
                    logging.info('New special key - {0}'.format(curr_text_obj.key))
                    curr_text_obj.value = [value]
                    logging.info('New special value - {0}'.format(value.text))
                    additional_text_objects.append(curr_text_obj)
            list_of_text_objects.extend(additional_text_objects)
        elif text_obj.key in special_urine_blood:
            found = False
            try:
                if list_of_text_objects[i-1].key.startswith('Urine'):
                    text_obj.key = 'Urine' + text_obj.key
                    found = True
                elif list_of_text_objects[i-1].key.startswith('Blood'):
                    text_obj.key = 'Blood' + text_obj.key
                    found = True
            except:
                pass
            if not found:
                try:
                    if list_of_text_objects[i+1].key.startswith('Urine'):
                        text_obj.key = 'Urine' + text_obj.key
                    elif list_of_text_objects[i+1].key.startswith('Blood'):
                        text_obj.key = 'Blood' + text_obj.key
                except:
                    pass
        elif text_obj.key == 'AdulterantTests' and text_obj.value is None:
            text_obj.value = [deepcopy(text_obj)]
            text_obj.value[0].text = re.sub('ADULTERANT TESTS ?', '', text_obj.value[0].text)

    list_of_key_objects = [text_obj for text_obj in list_of_text_objects if text_obj.key is not None and text_obj.key not in special_key_set]

    return list_of_key_objects


def intersect(coords_1, coords_2, how='horizontal'):
    if how == 'horizontal':
        return coords_1[1] <= coords_2[3] and coords_2[1] <= coords_1[3]
    elif how == 'vertical':
        return coords_1[0] <= coords_2[2] and coords_2[0] <= coords_1[2]
    elif how == 'both':
        return coords_1[0] <= coords_2[2] and coords_2[0] <= coords_1[2]\
                and coords_1[1] <= coords_2[3] and coords_2[1] <= coords_1[3]


def within_range(coords_1, coords_2, range=150, how='horizontal'):
    if how == 'horizontal':
        return abs(coords_1[0] - coords_2[2]) <= range or abs(coords_1[2] - coords_2[0]) <= range
    elif how == 'vertical':
        return abs(coords_1[1] - coords_2[3]) <= range or abs(coords_1[3] - coords_2[1]) <= range


def get_union(coords_1, coords_2):
    return min(coords_1[0], coords_2[0]), \
           min(coords_1[1], coords_2[1]), \
           max(coords_1[2], coords_2[2]), \
           max(coords_1[3], coords_2[3])


def static_var(varname, value):
    def decorate(func):
        setattr(func, varname, value)
        return func
    return decorate


class combine_text():
    def __init__(self):
        self.counter_dict = {}
        self.counter = 0

    def combine(self, coords, range=150):
        for key, val in self.counter_dict.items():
            if (intersect(coords, val) and within_range(coords, val, range)) or intersect(coords, val, how='both'):
                self.counter_dict[key] = get_union(coords, val)
                return key

        self.counter_dict[str(self.counter)] = coords
        self.counter += 1
        return str(self.counter - 1)


def combine_text_group(group_df):
    group_df.sort_values('x_pos')
    group_df.reset_index(inplace=True)
    curr_df = group_df.loc[0]
    curr_df['x_pos'] = group_df['x_pos'].min()
    curr_df['y_pos'] = group_df['y_pos'].min()
    curr_df['x_end'] = group_df['x_end'].max()
    curr_df['y_end'] = group_df['y_end'].max()
    curr_df['x_width'] = curr_df['x_end'] - curr_df['x_pos']
    curr_df['y_height'] = curr_df['y_end'] - curr_df['y_pos']

    group_df['truth_value'] = group_df['truth_value'].apply(str)
    curr_df['truth_value'] = group_df.groupby("text_group")['truth_value'].apply(' '.join)[0]
    # curr_df['confidence'] = (group_df['confidence'] * group_df['truth_value'].str.len()).sum() / max(group_df['truth_value'].str.len().sum(), 1)
    curr_df['box'] = [curr_df.x_pos, curr_df.y_pos, curr_df.x_end, curr_df.y_end]

    return curr_df.to_frame().transpose()


def combine_text_boxes(df):
    df = df.copy()
    df.sort_values('x_pos', inplace=True)

    avg_text_length = df['x_width'].sum() / df['truth_value'].str.len().sum()
    logging.info('Average text length: {0}'.format(avg_text_length))
    min_dist_same_text = avg_text_length * 3

    df['x_end'] = df['x_pos'] + df['x_width']
    df['y_end'] = df['y_pos'] + df['y_height']

    combine_text_ = combine_text()
    df['text_group'] = df.apply(lambda x: combine_text_.combine((x.x_pos, x.y_pos, x.x_end, x.y_end), min_dist_same_text), axis=1)

    list_of_combined_text_dfs = []
    for group_id, group in df.groupby('text_group'):
        list_of_combined_text_dfs.append(combine_text_group(group))

    combined_text_df = pd.concat(list_of_combined_text_dfs)

    return combined_text_df




