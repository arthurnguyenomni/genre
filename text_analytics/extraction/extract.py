import os
import pymysql
import dotenv
import pandas as pd
import boto3
from urllib.parse import urlparse
from io import BytesIO
from PIL import Image
import sys
from joblib import Parallel, delayed
import multiprocessing
import logging
logging.getLogger().setLevel(logging.DEBUG)

from text_analytics.extraction.transform import df_to_text_objects, get_keys, get_matches, combine_text_boxes, shrink_df
from tools.debugging import show_results, show_text, create_gt

TEMP_FOLDER = 'data/temp'
SYN_EXCEL = 'data/materials/important_keys.xlsx'


QUERY = '''
SELECT
    pipeline_form.group_id,
    pipeline_form.provider,
    pipeline_form.id AS form_id,
    text_check.form_id AS form_name,
    text_check.form_page,
    text_check.forms3link,
    pipeline_form.rotation_degrees,
    text_check.id AS text_check_id,
    text_check.x_pos,
    text_check.y_pos,
    text_check.x_width,
    text_check.y_height,
    text_check.truth_value,
    omni_key_data.omni_key_ref_id,
    omni_key_data.id AS key_id,
    omni_key_data.omni_key AS key_name,
    pipeline_form_value.value AS value_num
FROM text_check
LEFT OUTER JOIN pipeline_form_value
ON pipeline_form_value.text_check_id = text_check.id
LEFT OUTER JOIN omni_key_data
ON pipeline_form_value.key_data_id = omni_key_data.id
LEFT OUTER JOIN pipeline_form
ON text_check.form_ref_id = pipeline_form.id
WHERE text_check.label IS NULL
AND pipeline_form.form_status >=3
AND provider LIKE '%Gen%'
'''


def get_db_connection(project_dir):
    dotenv_path = os.path.join(project_dir, '.env')
    dotenv.load_dotenv(dotenv_path)

    db_host = os.environ.get('DB_HOST')
    db_user = os.environ.get('DB_USER')
    db_password = os.environ.get('DB_PASSWORD')

    db_connection = pymysql.connect(host=db_host,
                                    database='rga',
                                    user=db_user,
                                    password=db_password,
                                    charset='utf8')
    return db_connection


def extract(form, group, syn_df, shrink_size, output_dir, test):
    group = group.copy()
    logging.info(form)

    list_of_key_objects, list_of_text_objects = extract_data(group, syn_df, shrink_size=shrink_size, test=test)

    print('FORM-{0}\n'
          'TEXT OBJECTS-{1}'.format('-'.join(str(e) for e in form),
                                    len(list_of_text_objects)))

    input_image_path = os.path.join(TEMP_FOLDER,
                                    '{form_name}-{form_page}.png'.format(form_name=group.iloc[0]['form_name'],
                                                                         form_page=group.iloc[0]['form_page']))

    assert os.path.exists(input_image_path), logging.error('Image file does not exists for ({0}).\n'.format(input_image_path))

    if test:
        show_text(input_image_path, list_of_text_objects, output_dir)

    # debugging
    show_results(input_image_path, list_of_key_objects, output_dir)

    return create_gt('-'.join(str(e) for e in form), list_of_key_objects)


def batch_extract(df, syn_df, output_dir=None, shrink_size=1.0, test=True, parallelize=True):

    list_of_dfs = []

    # if parallelize:
    #     list_of_dfs = Parallel(n_jobs=multiprocessing.cpu_count())(delayed(extract)(form, group, syn_df, shrink_size, output_dir, test)
    #                                      for form, group in df.groupby(['form_name', 'form_page']))
    # else:
    #     for form, group in df.groupby(['form_name', 'form_page']):
    #         # if '-'.join(str(e) for e in form) == '5703413-lab_Redacted-2':
    #         group = group.copy()
    #         logging.info(form)
    #
    #         list_of_key_objects, list_of_text_objects = extract_data(group, syn_df, shrink_size=shrink_size, test=test)
    #
    #         print('FORM-{0}\n'
    #               'TEXT OBJECTS-{1}'.format('-'.join(str(e) for e in form),
    #                                         len(list_of_text_objects)))
    #
    #         input_image_path = os.path.join(TEMP_FOLDER, '{form_name}-{form_page}.png'.format(form_name=group.iloc[0]['form_name'],
    #                                                                                           form_page=group.iloc[0]['form_page']))
    #
    #         assert os.path.exists(input_image_path), logging.error('Image file does not exists for ({0}).\n')
    #
    #         if test:
    #             show_text(input_image_path, list_of_text_objects, output_dir)
    #
    #         # debugging
    #         show_results(input_image_path, list_of_key_objects, output_dir)
    #
    #         list_of_dfs.append(create_gt('-'.join(str(e) for e in form), list_of_key_objects))

    # if not test:
    #     df.to_pickle(os.path.join(output_dir, 'gt.pickle'))
    # else:
    #     df.to_pickle(os.path.join(output_dir, 'test.pickle'))

    list_of_key_objects, list_of_text_objects = extract_data(df, syn_df, shrink_size=shrink_size, test=False)

    df = pd.DataFrame(columns=['Key', 'Value'])
    for key in list_of_key_objects:
        df = df.append(pd.DataFrame([[key.text, key.value[0].text]], columns=df.columns))

    df.reset_index(inplace=True, drop=True)

    return df


def extract_data(df, syn_df, shrink_size=1.0, test=False):
    # label keys using syn df
    df = df.copy()
    df = shrink_df(df, shrink_size, axis=1)

    # if test:
    #     df = combine_text_boxes(df)

    df = shrink_df(df, shrink_size, axis=0)

    df = get_keys(df, syn_df)

    # df to text objects
    list_of_text_objects = df_to_text_objects(df)

    # get x_min and x_max
    x_min = min(list_of_text_objects, key=lambda x: x.x1).x1
    x_max = max(list_of_text_objects, key=lambda x: x.x2).x2

    logging.info('x_min:{min}'.format(min=x_min))
    logging.info('x_max:{max}'.format(max=x_max))

    list_of_key_objects = get_matches(list_of_text_objects, x_min, x_max)

    return list_of_key_objects, list_of_text_objects


def get_image_from_s3(url):
    """Downloads an image from s3 and returns as PIL Image."""
    url_parsed = urlparse(url)
    path = url_parsed.path
    path_split = path.split('/')
    bucket = path_split[1]
    bucket_path = '/'.join(path_split[2:])

    s3 = boto3.client('s3')
    f = BytesIO()
    s3.download_fileobj(bucket, bucket_path, f)
    img = Image.open(f)
    return img


def download_images(df):
    for iterrow in df.drop_duplicates('forms3link').itertuples():
        image = get_image_from_s3(iterrow.forms3link).rotate(-1.0*iterrow.rotation_degrees)
        output_file = os.path.join(TEMP_FOLDER, '{form_name}-{form_page}.png'.format(form_name=iterrow.form_name, form_page=iterrow.form_page))
        image.save(output_file)


def main(test=False, **kwargs):
    if test:
        # list_of_dfs = [pd.read_csv(os.path.join(kwargs['input_dfs'],
        #                                         input_path)) for input_path in os.listdir(kwargs['input_dfs'])]
        # df = pd.concat(list_of_dfs)
        # df = df[~pd.isnull(df['truth_value'])]
        df = pd.read_pickle(kwargs['df'])

    else:
        db_con = get_db_connection('.')
        df = pd.read_sql(QUERY,
                         con=db_con)

        df.to_pickle(os.path.join(TEMP_FOLDER, 'golden_truth_text.pickle'))
        if kwargs['download']:
            download_images(df)

    syn_df = pd.read_excel(SYN_EXCEL)

    syn_df = syn_df[syn_df['Type'] == 'Key']
    syn_df['regex_length'] = syn_df['RegEx'].str.len()
    syn_df = syn_df.sort_values(by='regex_length', ascending=False)

    output_dir = os.path.join(TEMP_FOLDER, 'testing')

    try:
        os.mkdir(output_dir)
    except:
        pass

    batch_extract(df, syn_df, output_dir, shrink_size=0.7, test=test, parallelize=kwargs['parallelize'])


def predict_df(df):
    syn_df = pd.read_excel(SYN_EXCEL)

    syn_df = syn_df[syn_df['Type'] == 'Key']
    syn_df['regex_length'] = syn_df['RegEx'].str.len()
    syn_df = syn_df.sort_values(by='regex_length', ascending=False)

    return batch_extract(df, syn_df, None, shrink_size=0.7, test=False, parallelize=False)


def clean_data(input_df_dir):
    for df_file in os.listdir(input_df_dir):
        input_df_file = os.path.join(input_df_dir, df_file)
        df = pd.read_csv(input_df_file)
        df['y_height'] = abs(df['y_height'])
        df['x_width'] = abs(df['x_width'])
        df.to_csv(input_df_file)


if __name__ == '__main__':
    # clean_data('/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/text/google_vision')

    # main(input_dfs='/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/text/tesseract',
    #      df='/Users/arthurnguyen/Desktop/Git-Omni/GenRe/data/temp/text/ocr_from_segmentation.pickle',
    #      parallelize=False,
    #      download=False,
    #      test=True)
    df = pd.read_csv('/Users/arthurnguyen/Desktop/Git-Omni/PoC/GenRe/data/temp/text/mega-ocr/5695447-lab_Redacted-0.csv')
    print(predict_df(df))
#

