class TextObject:
    def __init__(self,
                 text=None,
                 coords=None,
                 key=None,
                 value=None):
        self.text = text
        self.x1 = coords[0]
        self.y1 = coords[1]
        self.x2 = coords[2]
        self.y2 = coords[3]
        self.key = key
        self.value = value